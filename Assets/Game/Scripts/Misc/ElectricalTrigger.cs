using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ElectricalComponent))]
public class ElectricalTrigger : MonoBehaviour
{
    ElectricalComponent ec;

    [SerializeField]
    private bool PowerOnEnter = true;
    [SerializeField]
    private bool Latch = true;

    bool latched = false;
    // Start is called before the first frame update
    private void Awake()
    {
        ec = GetComponent<ElectricalComponent>();
        ec.IsPowering = !PowerOnEnter;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Latch)
        {
            latched = true;
            ec.IsPowering = PowerOnEnter;
        }
        else
        {
            ec.IsPowering = PowerOnEnter;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (Latch)
        {
            if (latched)
            {
                ec.IsPowering = PowerOnEnter;
            }
            else
            {
                ec.IsPowering = !PowerOnEnter;
            }
        }
        else
        {
            ec.IsPowering = !PowerOnEnter;
        }
    }
}
