using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlateOrder : MonoBehaviour
{
    public Animator animator1;
    public Animator animator2;
    public ElectricalComponent[] ElectricalComponents;

    // Start is called before the first frame update
    void Start()
    {
        ElectricalComponents[0].OnSignalEnable += PressurePlate1_OnSignalEnable;
        ElectricalComponents[1].OnSignalEnable += PressurePlate2_OnSignalEnable;
        ElectricalComponents[2].OnSignalEnable += PressurePlate3_OnSignalEnable;
        ElectricalComponents[3].OnSignalEnable += PressurePlate4_OnSignalEnable;
        ResetState();
    }
    [SerializeField]
    bool onePressed;
    [SerializeField]
    bool twoPressed;
    [SerializeField]
    bool threePressed;
    private void PressurePlate1_OnSignalEnable()
    {
        Debug.Log("One Pressed");
        onePressed = true;
    }
    private void PressurePlate2_OnSignalEnable()
    {
        if (onePressed)
        {
            Debug.Log("Two Pressed");
            twoPressed = true;
        }
        else
        {
            ResetState();
        }
    }
    private void PressurePlate3_OnSignalEnable()
    {
        if ((onePressed && twoPressed))
        {
            Debug.Log("Three Pressed");
            threePressed = true;
        }
        else
        {
            ResetState();
        }
    }
    private void PressurePlate4_OnSignalEnable()
    {
        if ((onePressed && twoPressed && threePressed))
        {
            OpenDoor();
        }
        else
        {
            ResetState();
        }
    }
    bool animate = false;
    void OpenDoor()
    {
        Debug.Log("Opening Exit Path");
        animator1.SetTrigger("Open");
        animator2.SetTrigger("Open"); animate = true;
        ActiveCameraController.Instance.SwitchTo(5, "PuzzleCompleteCam");
    }

    void ResetState()
    {
        onePressed = false;
        twoPressed = false;
        threePressed = false;
    }
    float timer;
    private void Update()
    {
        if (animate)
        {
            timer += Time.deltaTime;
            if (timer >= 7)
            {
                animate = false;
                GameObject.Destroy(ActiveCameraController.Instance.GetCam.gameObject);
                ActiveCameraController.Instance.ReloadCams();
                ActiveCameraController.Instance.SwitchToDefaultCam();
                ActiveCameraController.Instance.SwitchToDefaultCam();
            }
        }
    }
}
