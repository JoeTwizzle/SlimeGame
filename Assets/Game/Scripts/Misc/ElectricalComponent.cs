using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class ElectricalComponent : MonoBehaviour
{
    public List<ElectricalComponent> LinkedComponents = new List<ElectricalComponent>();

    public bool IsEmitter;

    public bool IsPowering;

    private bool WasRecievingSignal = false;

    private bool isRecievingSignal = false;

    public bool IsRecievingSignal
    {
        get
        {
            return isRecievingSignal;
        }
        set
        {
            WasRecievingSignal = isRecievingSignal;
            isRecievingSignal = value;
        }
    }

    public event Action OnSignalEnable;

    public event Action OnSignalDisable;

    public event Action OnSignalHigh;

    public event Action OnSignalLow;

    private void Update()
    {
        if (IsEmitter)
        {
            for (int i = 0; i < LinkedComponents.Count; i++)
            {
                if (LinkedComponents[i] == null)
                {
                    Debug.LogWarning("A LinkedComponent in " + name + " was null and has been removed");
                    LinkedComponents.RemoveAt(i);
                    i--;
                    continue;
                }
                LinkedComponents[i].IsRecievingSignal = IsPowering;
            }
        }
        if (IsRecievingSignal)
        {
            if (!WasRecievingSignal)
            {
                OnSignalEnable?.Invoke();
            }
            OnSignalHigh?.Invoke();
        }
        else
        {
            if (WasRecievingSignal)
            {
                OnSignalDisable?.Invoke();
            }
            OnSignalLow?.Invoke();
        }
    }
}
