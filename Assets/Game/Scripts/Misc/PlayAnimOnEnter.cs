using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimOnEnter : MonoBehaviour
{
    public Animator Animator;
    public Animator Credits;
    private void OnTriggerEnter(Collider other)
    {
        GameStateManager.Instance.SetState(GameState.Paused);
        ActiveCameraController.Instance.SwitchToPermanent("EndingVCam");
        Animator.SetTrigger("Open");
        Credits.SetTrigger("ScrollAnimator");
    }
    private void Update()
    {
        
    }
}
