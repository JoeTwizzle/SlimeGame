using System.Buffers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementDelta : MonoBehaviour
{
    public Vector3 Delta => delta;
    [SerializeField]
    private Vector3 delta;
    private Transform t;
    [SerializeField]
    private List<Rigidbody> RigidbodyList = new List<Rigidbody>();
    private Matrix4x4 prevMat;
    private Matrix4x4 prevFixedMat;
    private Vector3 prevPos;

    public void AddToList(Rigidbody rb)
    {
        RigidbodyList.Add(rb);
    }

    public void RemoveFromList(Rigidbody rb)
    {
        RigidbodyList.Remove(rb);
    }

    public Vector3 GetDelta(Vector3 pos)
    {
        var localPos = prevMat.MultiplyPoint3x4(pos);
        var worldPos = t.localToWorldMatrix.MultiplyPoint3x4(localPos);
        return worldPos - pos;
    }

    public Vector3 GetFixedDelta(Vector3 pos)
    {
        var localPos = prevFixedMat.MultiplyPoint3x4(pos);
        var worldPos = t.localToWorldMatrix.MultiplyPoint3x4(localPos);
        return worldPos - pos;
    }

    private void Awake()
    {
        t = transform;
        prevFixedMat = prevMat = t.worldToLocalMatrix;
        prevPos = t.position;
    }

    private void LateUpdate()
    {
        delta = t.position - prevPos;
        for (int i = 0; i < RigidbodyList.Count; i++)
        {
            var rb = RigidbodyList[i];
            var localPos = prevMat.MultiplyPoint3x4(rb.transform.position);
            var worldPos = t.localToWorldMatrix.MultiplyPoint3x4(localPos);
            rb.transform.position = worldPos;
        }
        prevPos = t.position;
        prevMat = t.worldToLocalMatrix;
    }

    private void FixedUpdate()
    {
        prevFixedMat = t.worldToLocalMatrix;
    }
}
