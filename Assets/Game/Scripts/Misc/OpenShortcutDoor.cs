using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenShortcutDoor : MonoBehaviour
{
    public Animator animator;
    public AudioSource source;
    ElectricalComponent pressurePlate;
    // Start is called before the first frame update
    void Start()
    {
        pressurePlate = GetComponent<ElectricalComponent>();
        pressurePlate.OnSignalEnable += PressurePlate_OnSignalEnable;
    }

    private void PressurePlate_OnSignalEnable()
    {
        delay = 1f;
        source.time = 0;
        source.Play();
        animator.SetTrigger("Open");
    }
    float delay = 0;
    private void Update()
    {
        if (source.isPlaying)
        {
            delay -= Time.deltaTime;
            if (delay<=0)
            {
                source.Stop();
            }
        }
    }
}
