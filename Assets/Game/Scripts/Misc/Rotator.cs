using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float Speed = 1f;
    public bool Reverse;
    [SerializeField] float angle;
    // Update is called once per frame
    void Update()
    {
        if (Reverse)
        {
            angle -= Time.deltaTime * Speed;
            if (angle <= 0)
            {
                angle = angle + 360f;
            }
        }
        else
        {
            angle += Time.deltaTime * Speed;
            angle %= 360f;
        }
        
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
    }
}
