using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public enum PathMode
{
    PingPong,
    Circle,
}

[ExecuteAlways]
public class PathFollow : MonoBehaviour
{
    [System.Serializable]
    public struct Node
    {
        public Vector3 Position;
        public float WaitDelay;
    }
    public List<Node> Nodes = new List<Node>();
    public float Speed = 1f;
    public PathMode PathMode;
    public bool Reverse;
    float waitTime;
    public Vector3 StaringPos;

    [SerializeField]
    int currentNodeIndex;
    [SerializeField]
    int nextNodeIndex;

    [SerializeField]
    float localPercentage;
    float totalPercentage;
    // Start is called before the first frame update
    private void Awake()
    {
        StaringPos = transform.position;
        currentNodeIndex = 0;
        nextNodeIndex = 1;
    }

    void Update()
    {
        if (!Application.isPlaying)
        {
            StaringPos = transform.position;
            return;
        }
        if (Nodes.Count < 2)
            return;
        //float alocalPercentage = totalPercentage * Nodes.Count % 1f;
        totalPercentage = (localPercentage + currentNodeIndex) / Nodes.Count;
        if (waitTime > 0)
        {
            waitTime -= Time.deltaTime;
            return;
        }
        float dist = Vector3.Distance(Nodes[currentNodeIndex].Position, Nodes[nextNodeIndex].Position);
        localPercentage += Time.deltaTime * Speed / dist;
        if (dist == 0)
        {
            localPercentage = 1;
        }
        if (localPercentage >= 1f)
        {
            currentNodeIndex = nextNodeIndex;
            int calcNextNodeIndex = nextNodeIndex;
            if (Reverse)
            {
                calcNextNodeIndex--;
            }
            else
            {
                calcNextNodeIndex++;
            }

            switch (PathMode)
            {
                case PathMode.PingPong:
                    if (calcNextNodeIndex >= Nodes.Count || calcNextNodeIndex < 0)
                    {
                        Reverse = !Reverse;
                    }
                    if (Reverse)
                    {
                        nextNodeIndex--;
                    }
                    else
                    {
                        nextNodeIndex++;
                    }
                    localPercentage = localPercentage % 1f;
                    break;
                case PathMode.Circle:
                    if (calcNextNodeIndex >= Nodes.Count)
                    {
                        nextNodeIndex = 0;
                    }
                    else if (calcNextNodeIndex < 0)
                    {
                        nextNodeIndex = Nodes.Count - 1;
                    }
                    else
                    {
                        nextNodeIndex = calcNextNodeIndex;
                    }
                    localPercentage = localPercentage % 1f;
                    break;
                default:
                    break;
            }
            waitTime = Nodes[currentNodeIndex].WaitDelay;
        }

        transform.position = StaringPos + Vector3.Lerp(Nodes[currentNodeIndex].Position, Nodes[nextNodeIndex].Position, localPercentage);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        for (int i = 0; i < Nodes.Count - 1; i++)
        {
            Gizmos.DrawLine(StaringPos + Nodes[i].Position, StaringPos + Nodes[i + 1].Position);
        }
        if (PathMode == PathMode.Circle)
        {
            if (Nodes.Count > 2)
            {
                Gizmos.DrawLine(StaringPos + Nodes[0].Position, StaringPos + Nodes[^1].Position);
            }
        }
    }
}
