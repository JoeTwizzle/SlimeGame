using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnPlayer : MonoBehaviour
{
    public float KillPlaneHeight = -100f;
    private Vector3 SpawnPoint;
    public Animator DeathFadeAnimator;
    public Animator CheckpointAnimator;
    const float FadeTime = 0.45f;
    bool respawn;
    float FadeCountDown = 0;

    public Vector3 GetSpawnPoint()
    {
        return SpawnPoint;
    }

    public void SetSpawnPoint(Vector3 point)
    {
        if (SpawnPoint != point)
        {
            SpawnPoint = point;
            CheckpointAnimator.SetTrigger("CheckpointGet");
        }
    }

    public void Respawn()
    {
        if (!respawn)
        {
            FadeCountDown = FadeTime;
            respawn = true;
            DeathFadeAnimator.SetTrigger("FadeIn");
        }
    }

    void ProcessRespawnTimer()
    {
        if (respawn)
        {
            FadeCountDown -= Time.deltaTime;
            if (FadeCountDown <= 0)
            {
                transform.position = SpawnPoint;
                DeathFadeAnimator.SetTrigger("FadeOut");
                respawn = false;
            }
        }
    }

    private void Update()
    {
        ProcessRespawnTimer();
        if (transform.position.y <= KillPlaneHeight)
        {
            if (!respawn)
            {
                Respawn();
            }
        }
    }
}
