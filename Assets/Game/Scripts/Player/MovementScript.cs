using UnityEngine;

public static class Vector3Extensions
{
    /// <summary>
    /// Inverts a scale vector by dividing 1 by each component
    /// </summary>
    public static Vector3 Invert(this Vector3 vec)
    {
        return new Vector3(1 / vec.x, 1 / vec.y, 1 / vec.z);
    }
}

[RequireComponent(typeof(Rigidbody))]
public class MovementScript : MonoBehaviour
{
    public AudioSource LandSource;
    public AudioSource JumpSource;
    public CameraController CameraController;
    public LayerMask SolidMask;
    [SerializeField]
    private GameObject visual;
    [SerializeField]
    private float moveAccel = 6f;
    [SerializeField]
    private float maxMoveSpeed = 6f;
    [SerializeField]
    private float jumpStrength = 10f;
    [SerializeField]
    private float groundedDistance = 0.6f;
    [SerializeField]
    private float airControlFactor = 0.4f;
    [SerializeField]
    private float jumpTimeInAir = 0.2f;
    [SerializeField]
    private float gravity = 30f;
    private Rigidbody rb;
    private Transform cameraTransform;
    private Transform lastTransform;
    private Transform visualTransform;
    private MovementDelta movementDelta;
    private SkinnedMeshRenderer meshRenderer;
    private Mesh mesh;


    private readonly string[] blendShapes = new string[] { "SquishLeft", "SquishRight", "SquishFront", "SquishBack" };
    private int[] blendShapeIndices;
    private readonly Vector3[] directions = new Vector3[] { Vector3.left, Vector3.right, Vector3.forward, Vector3.back };
    private int stretchIndex;
    private int vSquishIndex;


    public static float RemapRange(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

    private void Awake()
    {
        visualTransform = visual.transform;
        meshRenderer = visual.GetComponent<SkinnedMeshRenderer>();
        mesh = meshRenderer.sharedMesh;
        blendShapeIndices = new int[blendShapes.Length];
        for (int i = 0; i < blendShapes.Length; i++)
        {
            blendShapeIndices[i] = mesh.GetBlendShapeIndex(blendShapes[i]);
        }
        vSquishIndex = mesh.GetBlendShapeIndex("SquishDown");
        stretchIndex = mesh.GetBlendShapeIndex("StretchVert");
        rb = GetComponent<Rigidbody>();
        cameraTransform = CameraController.transform;
    }
    float timeInAir;
    Vector2 moveDir;
    bool wantJump;
    float directionalSquishAmount;
    float verticalSquishAmount;
    float verticalStretchAmount;
    // Update is called once per frame
    void Update()
    {
        if (GameStateManager.Instance.GameState == GameState.Running)
        {
            rb.constraints = RigidbodyConstraints.FreezeRotation;
        }
        else
        {
            rb.constraints = RigidbodyConstraints.FreezeAll;
            return;
        }
        moveDir.x = Input.GetAxis("Horizontal");
        moveDir.y = Input.GetAxis("Vertical");
        wantJump = Input.GetAxis("Jump") > 0;
        for (int i = 0; i < blendShapeIndices.Length; i++)
        {
            Vector3 transformedDir = visualTransform.TransformDirection(directions[i]);
            Ray ray = new Ray(rb.position, transformedDir);
            if (Physics.Raycast(ray, out RaycastHit hit, 1f, SolidMask))
            {
                Vector2 forward = new Vector2(cameraTransform.forward.x, cameraTransform.forward.z).normalized;
                Vector2 right = new Vector2(cameraTransform.right.x, cameraTransform.right.z).normalized;
                Vector2 horizontalWishDir = (right * moveDir.x + forward * moveDir.y).normalized;
                float val = Mathf.Max(0, Vector2.Dot(horizontalWishDir, transformedDir));
                if (val > 0)
                {
                    directionalSquishAmount = Mathf.Min(directionalSquishAmount + 3f * Time.deltaTime, 1);
                }
                else
                {
                    directionalSquishAmount = Mathf.Max(directionalSquishAmount - 0.5f * Time.deltaTime, 0);
                }

                meshRenderer.SetBlendShapeWeight(blendShapeIndices[i], directionalSquishAmount * 100);
            }
            else
            {
                directionalSquishAmount = Mathf.Max(directionalSquishAmount - 0.5f * Time.deltaTime, 0);
                meshRenderer.SetBlendShapeWeight(blendShapeIndices[i], 0f);
            }
        }

        Ray downRay = new Ray(rb.position, Vector3.down);
        bool visualGrounded = Physics.SphereCast(downRay, 0.1f, out _, groundedDistance, SolidMask);
        if (visualGrounded && !wasGrounded)
        {
            LandSource.Play();
            verticalSquishAmount = Mathf.Clamp01(Mathf.InverseLerp(0, -5f, rb.velocity.y));
        }
        else
        {
            verticalSquishAmount = Mathf.Max(verticalSquishAmount - 2.5f * Time.deltaTime, 0);
        }
        if (!visualGrounded && wasGrounded)
        {
            JumpSource.Play();
            verticalStretchAmount = Mathf.Clamp01(Mathf.InverseLerp(0, 5f, rb.velocity.y));
        }
        else
        {
            verticalStretchAmount = Mathf.Max(verticalStretchAmount - 2.8f * Time.deltaTime, 0);
        }
        meshRenderer.SetBlendShapeWeight(vSquishIndex, Easing.ExponentialEaseOut(verticalSquishAmount) * 100);
        meshRenderer.SetBlendShapeWeight(stretchIndex, Easing.ExponentialEaseOut(verticalStretchAmount) * 100);
        wasGrounded = visualGrounded;
    }
    [SerializeField]
    Vector3 externalVelocity = new Vector3();
    bool wasGrounded;
    void FixedUpdate()
    {
        if (GameStateManager.Instance.GameState != GameState.Running)
        {
            return;
        }

        var velocity = rb.velocity;
        Vector2 forward = new Vector2(cameraTransform.forward.x, cameraTransform.forward.z).normalized;
        Vector2 right = new Vector2(cameraTransform.right.x, cameraTransform.right.z).normalized;
        Vector2 horizontalWishVel = (right * moveDir.x + forward * moveDir.y) * maxMoveSpeed;
        var hvel = new Vector2(velocity.x, velocity.z);
        Ray downRay = new Ray(rb.position, Vector3.down);
        bool isGrounded = Physics.SphereCast(downRay, 0.1f, out RaycastHit hit, groundedDistance, SolidMask);
        if (isGrounded)
        {
            externalVelocity = Vector3.zero;
            timeInAir = 0;
            hvel = Vector2.Lerp(hvel, horizontalWishVel, moveAccel * Time.fixedDeltaTime);
            if (lastTransform != hit.transform)
            {
                lastTransform = hit.transform;
                if (movementDelta != null)
                {
                    movementDelta.RemoveFromList(rb);
                    movementDelta = null;
                }
                if (lastTransform.TryGetComponent(out movementDelta))
                {
                    movementDelta.AddToList(rb);
                }
            }

            velocity = new Vector3(hvel.x, velocity.y, hvel.y);

        }
        else
        {
            lastTransform = null;
            timeInAir += Time.fixedDeltaTime;
            hvel = Vector2.Lerp(hvel, horizontalWishVel, moveAccel * Time.fixedDeltaTime * airControlFactor);
            //If we are in air and have movementDelta
            if (movementDelta != null)
            {
                //Dont work for some reason
                externalVelocity += movementDelta.GetFixedDelta(transform.position);
                movementDelta.RemoveFromList(rb);
                movementDelta = null;
            }
        }
        if ((isGrounded || timeInAir <= jumpTimeInAir) && wantJump)
        {
            timeInAir += 10000;
            velocity.y = jumpStrength;
        }
        hvel = Vector2.ClampMagnitude(hvel, maxMoveSpeed);
        velocity = new Vector3(hvel.x, velocity.y, hvel.y);
        if (horizontalWishVel != Vector2.zero)
        {
            if (CameraController.IsZTarget)
            {
                visualTransform.LookAt(visualTransform.position + new Vector3(forward.x, 0, forward.y));
            }
            else
            {
                visualTransform.LookAt(visualTransform.position + new Vector3(hvel.x, 0, hvel.y));
            }
        }
        if (!isGrounded && wantJump)
        {
            velocity += 0.8f * gravity * Time.fixedDeltaTime * Vector3.down;
        }
        else
        {
            velocity += gravity * Time.fixedDeltaTime * Vector3.down;
        }

        velocity += externalVelocity;
        rb.velocity = velocity;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, transform.position + externalVelocity);
    }
}
