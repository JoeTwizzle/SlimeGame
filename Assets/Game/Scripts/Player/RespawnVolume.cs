using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnVolume : MonoBehaviour
{
    public float AreaKillPlaneHeight;
    public bool SetKillPlaneHeight;
    [SerializeField]
    LayerMask notPlayer;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<RespawnPlayer>(out var respawnPlayer))
        {
            var downRay = new Ray(transform.position, Vector3.down);
            if (Physics.Raycast(downRay, out RaycastHit hit, 5, notPlayer))
            {
                respawnPlayer.SetSpawnPoint(hit.point);
            }
            else
            {
                respawnPlayer.SetSpawnPoint(transform.position);
            }
            if (SetKillPlaneHeight)
            {
                respawnPlayer.KillPlaneHeight = AreaKillPlaneHeight;
            }
        }
    }
}
