using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform LookTarget;
    public Transform PlayerVisualTarget;
    public Cinemachine.CinemachineVirtualCamera VirtualCamera;
    public Vector2 Sensitivity = Vector2.one;
    public bool InvertY = false;
    public bool InvertX = false;

    [SerializeField]
    public float ControllerCameraSpeed = 3;

    public bool IsZTarget => ztarget;
    bool ztarget;
    Vector2 moveXY;

    [SerializeField]
    Vector2 desiredXY;

    private void Start()
    {
        ActiveCameraController.Instance.ReloadCams();
        ActiveCameraController.Instance.SwitchToDefaultCam();
    }

    static float Greatest(float a, float b)
    {
        if (Mathf.Abs(a) > Mathf.Abs(b))
        {
            return a;
        }
        else
        {
            return b;
        }
    }
    float time = 0;
    // Update is called once per frame
    void Update()
    {
        if (LookTarget != null)
        {
            if (GameStateManager.Instance.GameState == GameState.Running)
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
                return;
            }
            ztarget = Input.GetAxis("Fire1") > 0;
            if (IsZTarget)
            {
                desiredXY = new Vector2(PlayerVisualTarget.transform.rotation.eulerAngles.y, 0);
            }
            moveXY.x = Greatest(Input.GetAxis("Mouse X"), Input.GetAxis("CamX") * ControllerCameraSpeed) * (InvertX ? -1 : 1);
            moveXY.y = Greatest(Input.GetAxis("Mouse Y"), Input.GetAxis("CamY") * ControllerCameraSpeed) * (InvertY ? 1 : -1);
            desiredXY += moveXY * Sensitivity;
            desiredXY.y = Mathf.Min(desiredXY.y, 89.9f);
            desiredXY.y = Mathf.Max(desiredXY.y, -89.9f);
            desiredXY.x %= 360f;
            Quaternion newRotation = Quaternion.Euler(desiredXY.y, desiredXY.x, 0);
            LookTarget.rotation = Quaternion.Slerp(LookTarget.rotation, newRotation, 0.25f);
        }
    }
}
