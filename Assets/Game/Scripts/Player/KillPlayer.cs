using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlayer : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<RespawnPlayer>(out var respawnPlayer))
        {
            respawnPlayer.Respawn();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent<RespawnPlayer>(out var respawnPlayer))
        {
            respawnPlayer.Respawn();
        }
    }
}
