using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    Paused,
    Running
}

public class GameStateManager : MonoSingleton<GameStateManager>
{
    GameState state;
    public GameState GameState => state;
    public event Action<GameState, GameState> OnStateChange;

    private void Awake()
    {
        state = GameState.Running;
    }

    public void SetState(GameState state)
    {
        var oldState = this.state;
        this.state = state;
        OnStateChange?.Invoke(oldState, state);
    }
}
