using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.SceneManagement;

public class ActiveCameraController : MonoSingleton<ActiveCameraController>
{
    [SerializeField]
    string DefaultCamName;
    Dictionary<string, CinemachineVirtualCamera> cameras = new Dictionary<string, CinemachineVirtualCamera>();
    bool animate;

    float elapsed;
    float duration;
    string activeCam;
    CinemachineVirtualCamera activeCamObj;

    public void SwitchTo(float duration, string camName)
    {
        animate = true;
        this.duration = duration;
        elapsed = 0;
        activeCamObj = cameras[camName];
        cameras[DefaultCamName].Priority = 10;
        activeCamObj.Priority = 100;
        activeCam = camName;
    }

    public void SwitchToPermanent(string CamName)
    {
        activeCam = CamName;
        cameras[DefaultCamName].Priority = 10;
        activeCamObj = cameras[CamName];
        activeCamObj.Priority = 100;
    }



    public void SwitchToDefaultCam()
    {
        foreach (var cam in cameras)
        {
            cam.Value.Priority = 5;
        }
        if (cameras.TryGetValue(DefaultCamName, out var camera))
        {
            activeCamObj = camera;
            camera.Priority = 100;
        }
        else if (cameras.TryGetValue("PlayerCamera", out var camera2))
        {
            activeCamObj = camera2;
            camera2.Priority = 100;
        }
    }

    public float Progress => Mathf.Clamp01(elapsed / duration);

    public bool DefaultCamActive => activeCam == DefaultCamName;

    public CinemachineVirtualCamera GetCam => activeCamObj;

    private void Awake()
    {
        animate = false;
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        SceneManager.sceneUnloaded += SceneManager_sceneUnloaded;
        ReloadCams();
    }

    private void SceneManager_sceneUnloaded(Scene scene)
    {
        ReloadCams();
    }

    private void SceneManager_sceneLoaded(Scene scene, LoadSceneMode mode)
    {
        ReloadCams();
    }

    public void ReloadCams()
    {
        var cams = FindObjectsOfType<CinemachineVirtualCamera>();
        if (cameras == null)
        {
            cameras = new Dictionary<string, CinemachineVirtualCamera>();
        }
        else
        {
            cameras.Clear();
        }
        foreach (var cam in cams)
        {
            cameras.TryAdd(cam.name, cam);
        }
    }

    private void Update()
    {
        if (animate)
        {
            elapsed += Time.deltaTime;
            if (elapsed >= duration)
            {
                elapsed = 0;
                animate = false;
                SwitchToDefaultCam();
            }
        }
    }
}
