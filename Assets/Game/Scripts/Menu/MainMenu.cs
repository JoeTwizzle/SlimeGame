using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public void StartGame()
    {
        SceneManager.LoadScene("DemoScene", LoadSceneMode.Single);
        ActiveCameraController.Instance.SwitchToDefaultCam();
        GameStateManager.Instance.SetState(GameState.Running);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    private void Update()
    {
        if (Input.GetButton("Jump"))
        {
            SceneManager.LoadScene("DemoScene", LoadSceneMode.Single);
            ActiveCameraController.Instance.SwitchToDefaultCam();
            GameStateManager.Instance.SetState(GameState.Running);
        }
    }
}
