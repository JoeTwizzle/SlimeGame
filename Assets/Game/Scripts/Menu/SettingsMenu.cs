using UnityEngine.Audio;
using UnityEngine;

public class SettingsMenu : MonoBehaviour
{
    public AudioMixer audioMixer;
   public void SetQuality(int qualityIndeex)
    {
        QualitySettings.SetQualityLevel(qualityIndeex);
    }
    public void SetMasterVolume(float volume)
    {
        audioMixer.SetFloat("MasterVolume", volume);
    }
    public void SetMusicVolume(float volume)
    {
        audioMixer.SetFloat("Music", volume);
    }
    public void SetEffectVolume(float volume)
    {
        audioMixer.SetFloat("Effects", volume);
    }
}
