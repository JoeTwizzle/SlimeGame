using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PathFollow))]
public class PathFollowEditor : Editor
{
    private void OnSceneGUI()
    {
        var pathFollow = target as PathFollow;
        var pos = pathFollow.StaringPos;
        for (int i = 0; i < pathFollow.Nodes.Count; i++)
        {
            var node = pathFollow.Nodes[i];
            node.Position = Handles.PositionHandle(pos + node.Position, Quaternion.identity) - pos;
            pathFollow.Nodes[i] = node;
        }

    }
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Subtract Position"))
        {
            var pathFollow = target as PathFollow;
            var pos = pathFollow.StaringPos;
            for (int i = 0; i < pathFollow.Nodes.Count; i++)
            {
                var node = pathFollow.Nodes[i];
                node.Position -= pos;
                pathFollow.Nodes[i] = node;
            }
        }
        if (GUILayout.Button("Add Position"))
        { 
            var pathFollow = target as PathFollow;
            var pos = pathFollow.StaringPos;
            for (int i = 0; i < pathFollow.Nodes.Count; i++)
            {
                var node = pathFollow.Nodes[i];
                node.Position += pos;
                pathFollow.Nodes[i] = node;
            }
        }
    }
}
